﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using OutReachBAL;
using OutReachBusinessEntities;
using OutReachIService;
using OutReachUtilities;

namespace OutReachAPI.Controllers
{
    [RoutePrefix("UploadExcel")]
    public class OutReachController : ApiController
    {
        private IUploadExcelBAL UploadExcel
        {
            get
            {
                return UnityInstanceProvider.Resolve<IUploadExcelBAL>();
            }
        }

        [HttpPost]
        [Route("OutReachAssociate")]
        public IHttpActionResult OutReachAssociate()
        {
            long returnCode = -1;
            int flag = 1;
            returnCode = OutReachBulkUpload(flag);
            if (returnCode >= 0)
                return Ok(returnCode);
            else
                return BadRequest();
        }
        [HttpPost]
        [Route("OREventSummary")]
        public IHttpActionResult OutReachSummary()
        {
            long returnCode = -1;
            int flag = 2;
            returnCode = OutReachBulkUpload(flag);
            if (returnCode >= 0)
                return Ok(returnCode);
            else
                return BadRequest();
        }
        [HttpPost]
        [Route("OREventInfo")]
        public IHttpActionResult OutReachInfo()
        {
            long returnCode = -1;
            int flag = 3;
            returnCode = OutReachBulkUpload(flag);
            if (returnCode >= 0)
                return Ok(returnCode);
            else
                return BadRequest();

        }
        private long OutReachBulkUpload(int flag)
        {
            long returnCode = -1;
           
            UploadExcelBAL objExcelBAL = new UploadExcelBAL();
            string path = ConfigurationManager.AppSettings["Path"];
            if (!string.IsNullOrEmpty(path))
            {
                var httpRequest = HttpContext.Current.Request;
                if (httpRequest.Files.Count > 0)
                {
                    foreach (string file in httpRequest.Files)
                    {
                        var postedFile = httpRequest.Files[file];
                        //var filePath = HttpContext.Current.Server.MapPath("~/UploadFile/" + postedFile.FileName);
                        postedFile.SaveAs(path + postedFile.FileName);
                        returnCode = objExcelBAL.InsertAssociateDetails(path + postedFile.FileName, flag);

                    }
                }

            }
            return returnCode;
         
        }
        [HttpGet]
        [Route("EngagementMetrics")]
        public IHttpActionResult Charts()
        {
            Metrics objMetrics = new Metrics();
            List<Charts> lstMetrics = objMetrics.GetCharts();
            c obJCharts = new c();
           
           
            foreach (var item in lstMetrics)
            {

               
                obJCharts.FrequencyType = lstMetrics.Select(I => Convert.ToString(I.FrequencyType)).ToArray();
                obJCharts.FrequencyPercentage = lstMetrics.Select(I => Convert.ToInt64(I.FrequencyPercentage)).ToArray();

            }
            obJCharts.lstEngageCharts = lstMetrics;
          

            if (obJCharts.lstEngageCharts.Count > 0 && obJCharts.lstEngageCharts != null)
                return Ok(obJCharts);
            else
                return NotFound();
        }
        [HttpGet]
        [Route("RentationMetrics")]
        public IHttpActionResult ORRentationMetrics()
        {
            Metrics objMetrics = new Metrics();
            ReMetrics objReMetrics = new ReMetrics();
            List<RetentationMetrics> lstMetrics = objMetrics.GetRentationMeterics();
            c obJCharts = new c();


            foreach (var item in lstMetrics)
            {


                objReMetrics.VPercenMonth = lstMetrics.Select(I => Convert.ToInt64(I.VPercenMonth)).ToArray();
                objReMetrics.VPercenQuarter = lstMetrics.Select(I => Convert.ToInt64(I.VPercenQuarter)).ToArray();
                objReMetrics.LastVDay = lstMetrics.Select(I => Convert.ToDateTime(I.LastVDay)).ToArray();
                objReMetrics.labelReMetrics = lstMetrics.Select(I => Convert.ToString(I.labelReMetrics)).ToArray();

            }
            objReMetrics.lstReMetrics = lstMetrics;


            if (objReMetrics.lstReMetrics.Count > 0 && objReMetrics.lstReMetrics != null)
                return Ok(objReMetrics);
            else
                return NotFound();
        }

    }
}
