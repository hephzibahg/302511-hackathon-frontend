﻿using System.Collections.Generic;
using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using OutReachDAL;
using OutReachIService;
using OutReachBusinessEntities;
namespace OutReachBAL
{
    public class UploadExcelBAL:IUploadExcelBAL
    {
        UploadExcelDAL objExcelDAL = new UploadExcelDAL();
       
        public long InsertAssociateDetails(string path,int flag)
        {
            long returnCode = -1;
            try
            {
                DataTable dt = ConvertExcelToDataTable(path);
                returnCode = objExcelDAL.InsertAssociateDetails(dt,flag);
            }
            catch (Exception ex)
            {
                returnCode = -1;
            }
            return returnCode;
        }
        private static DataTable ConvertExcelToDataTable(string FileName)
        {
            DataTable dtResult = null;


            int totalSheet = 0; //No of sheets on excel file  
            using (OleDbConnection objConn = new OleDbConnection(@"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + FileName + ";Extended Properties='Excel 12.0;HDR=YES;IMEX=1;';"))
            {
                objConn.Open();
                OleDbCommand cmd = new OleDbCommand();
                OleDbDataAdapter oleda = new OleDbDataAdapter();
                DataSet ds = new DataSet();
                DataTable dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                string sheetName = string.Empty;
                if (dt != null)
                {
                    var tempDataTable = (from dataRow in dt.AsEnumerable()
                                         where !dataRow["TABLE_NAME"].ToString().Contains("FilterDatabase")
                                         select dataRow).CopyToDataTable();
                    dt = tempDataTable;
                    totalSheet = dt.Rows.Count;
                    sheetName = dt.Rows[0]["TABLE_NAME"].ToString();
                }
                cmd.Connection = objConn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + sheetName + "]";
                oleda = new OleDbDataAdapter(cmd);
                oleda.Fill(ds, "excelData");
                dtResult = ds.Tables["excelData"];
                objConn.Close();
                //Returning Dattable  
            }

            return dtResult;
        }
    }
}
