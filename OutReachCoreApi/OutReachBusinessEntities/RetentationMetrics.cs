﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutReachBusinessEntities
{
    public class RetentationMetrics
    {
        public int VPercenQuarter { get; set; }
        public int VPercenMonth { get; set; }
        
        public DateTime LastVDay{ get; set; }
        public string labelReMetrics { get; set; }

    }
   
    public class ReMetrics
    {
        public long[] VPercenMonth { get; set; }
        public long[] VPercenQuarter { get; set; }
        public DateTime[] LastVDay { get; set; }
        public string[] labelReMetrics { get; set; }
        public List<RetentationMetrics> lstReMetrics { get; set; }

    }
}
