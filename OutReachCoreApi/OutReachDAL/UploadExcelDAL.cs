﻿using OutReachBusinessEntities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
namespace OutReachDAL
{
    public class UploadExcelDAL
    {
        string conStr = ConfigurationManager.ConnectionStrings["conStr"].ConnectionString.ToString();
        public long InsertAssociateDetails(DataTable dt, int flag)
        {

            long returnCode = -1;
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = con,
                    CommandText = "SP_Insert_AssociateDetails",
                    CommandType = CommandType.StoredProcedure

                };
                SqlParameter sqlParam = new SqlParameter();
                if (flag.Equals(1) && dt.Rows.Count > 0)
                {
                    sqlParam = cmd.Parameters.AddWithValue("@InsertAssociateDetails", dt);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }

                else { 
                    sqlParam = cmd.Parameters.AddWithValue("@InsertAssociateDetails", null);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }

                if (flag.Equals(2) && dt.Rows.Count > 0) { 
                    sqlParam = cmd.Parameters.AddWithValue("@InsertOREventInfo", dt);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }
                else { 
                    sqlParam = cmd.Parameters.AddWithValue("@InsertOREventInfo", null);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }


                if (flag.Equals(3) && dt.Rows.Count > 0)
                {

                    sqlParam = cmd.Parameters.AddWithValue("@InsertORSummary", dt);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }
                else { 
                    sqlParam = cmd.Parameters.AddWithValue("@InsertORSummary", null);
                    sqlParam.SqlDbType = SqlDbType.Structured;
                }


                //sqlParam.SqlDbType = SqlDbType.Structured;
                cmd.Parameters.AddWithValue("@flag",flag);
                cmd.Parameters.Add("@Id", SqlDbType.Int);
                cmd.Parameters["@Id"].Direction = ParameterDirection.Output;
                cmd.ExecuteNonQuery();
                returnCode = (Int32)cmd.Parameters["@Id"].Value;
            }
            return returnCode;
        }

        public List<Charts> GetEngageMretricsDAL()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = con,
                    CommandText = "SP_Engagemetrics",
                    CommandType = CommandType.StoredProcedure

                };
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            List<Charts> lstMetrics = null;


            lstMetrics = (from DataRow dr in dt.Rows
                          select new Charts()
                          {

                              FrequencyPercentage = Convert.ToInt64(dr["FrequencyPercentage"]),
                              FrequencyType = (dr["FrequencyType"]).ToString(),

                          }).ToList();
            return lstMetrics;
        }
        public List<RetentationMetrics> GetRetentationMetricsDAL()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(conStr))
            {
                con.Open();
                SqlCommand cmd = new SqlCommand
                {
                    Connection = con,
                    CommandText = "SP_RetentationMetrics",
                    CommandType = CommandType.StoredProcedure

                };
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                sda.Fill(dt);
            }
            List<RetentationMetrics> lstMetrics = null;


            lstMetrics = (from DataRow dr in dt.Rows
                          select new RetentationMetrics()
                          {

                              VPercenQuarter = Convert.ToInt32(dr["VPercenQuarter"]),
                              VPercenMonth = Convert.ToInt32(dr["VPercenMonth"]),
                              LastVDay = Convert.ToDateTime(dr["LastVDay"]),
                              labelReMetrics = Convert.ToString(dr["labelReMetrics"])

                          }).ToList();
            return lstMetrics;
        }
    }
}
