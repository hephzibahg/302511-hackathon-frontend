﻿
using System;
using Unity;
using OutReachIService;
namespace OutReachUtilities
{
    public static class UnityInstanceProvider
    {
        private static IUnityContainer _container;
        public static IUnityContainer Container
        {
            get { return _container; }
            private set { _container = value; }
        }
        static UnityInstanceProvider()
        {
            try
            {
                var container = new UnityContainer();
                RegisterTypes(container);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        private static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterType<IUploadExcelBAL>();
        }
        public static T Resolve<T>() {
            return Container.Resolve<T>();
        }
    }
}
